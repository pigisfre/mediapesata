package it.unibas.mediapesataswing.controllo;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.modello.Studente;
import it.unibas.mediapesataswing.vista.PannelloPrincipale;
import it.unibas.mediapesataswing.vista.Vista;

public class AzioneCalcolaMedia extends javax.swing.AbstractAction {
    
    private Controllo controllo;
    
    public AzioneCalcolaMedia(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Calcola Media");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Calcola la media pesata in 30mi e in 110mi");
        this.putValue(javax.swing.Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_C));
        this.putValue(javax.swing.Action.ACCELERATOR_KEY, javax.swing.KeyStroke.getKeyStroke("ctrl C"));
    }
    
    public void actionPerformed(java.awt.event.ActionEvent e) {
        Modello modello = this.controllo.getModello();
        Vista vista = this.controllo.getVista();
        PannelloPrincipale pannello = (PannelloPrincipale)vista.getSottoVista(Costanti.VISTA_PANNELLO_PRINCIPALE);
        Studente studente = (Studente)modello.getBean(Costanti.STUDENTE);
        if (studente == null) {
            this.controllo.getVista().finestraErrore("Studente nullo");
        } else if (studente.getNumeroEsami() == 0) {
            this.controllo.getVista().finestraErrore("Non ci sono esami su cui calcolare la media");            
        } else {
            pannello.schermoMedia();
        }
    }
    
}
