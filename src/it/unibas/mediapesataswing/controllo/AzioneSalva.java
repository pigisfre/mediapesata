package it.unibas.mediapesataswing.controllo;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.modello.Studente;
import it.unibas.mediapesataswing.persistenza.DAOException;
import it.unibas.mediapesataswing.persistenza.DAOStudente;
import java.io.File;
import javax.swing.JFileChooser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AzioneSalva extends javax.swing.AbstractAction {

    private Log logger = LogFactory.getLog(this.getClass());
    private Controllo controllo;

    public AzioneSalva(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Salva");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Salva i dati dello studente sul disco");
        this.putValue(javax.swing.Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_S));
        this.putValue(javax.swing.Action.ACCELERATOR_KEY, javax.swing.KeyStroke.getKeyStroke("ctrl S"));
    }

    public void actionPerformed(java.awt.event.ActionEvent e) {
        String nomeFile = acquisisciFile();
        if (nomeFile != null) {
            try {
                salvaDatiStudente(nomeFile);
            } catch (DAOException daoe) {
                logger.error(daoe);
                this.controllo.getVista().finestraErrore("Impossibile salvare il file " + daoe);
            }
        }
    }

    private String acquisisciFile() {
        JFileChooser fileChooser = this.controllo.getVista().getFileChooser();
        int codice = fileChooser.showSaveDialog(this.controllo.getVista());
        if (codice == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            return file.toString();
        } else {
            logger.info("Comando salva annullato");
        }
        return null;
    }

    private void salvaDatiStudente(String nomeFile) throws DAOException {
        Modello modello = this.controllo.getModello();
        Studente studente = (Studente) modello.getBean(Costanti.STUDENTE);
        DAOStudente.salva(studente, nomeFile);
    }
    
}
