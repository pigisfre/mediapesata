package it.unibas.mediapesataswing.modello;

import static org.junit.Assert.*;



import org.junit.Test;

public class StudenteTest {

	@Test
	public void testGetEsame() throws Exception {
		Studente studente=new Studente("Pierluigi","Sfregola",655689);
		Esame exam=new Esame ("piu",30,false,9);
		studente.addEsame(exam);
		assertEquals(exam,studente.getEsame(0));
		
		
	}
	
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void testGetEsameNegative() throws Exception {
		Studente studente=new Studente("Pierluigi","Sfregola",655689);
		studente.getEsame(-1);
		
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void testGetEsameExcep() throws Exception {
		Studente studente=new Studente("Pierluigi","Sfregola",655689);
		studente.getEsame(1);
		
	}
	
	
	
	@Test
	public void testEliminaEsame() throws Exception {
		Studente studente=new Studente("Pierluigi","Sfregola",655689);
		Esame exam=new Esame ("piu",30,false,9);
		studente.addEsame(exam);
		studente.eliminaEsame(0);
		assertEquals(0,studente.getNumeroEsami());
		
		
	}
	
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void testEliminasameNegative() throws Exception {
		Studente studente=new Studente("Pierluigi","Sfregola",655689);
		studente.eliminaEsame(-1);
		
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void testEliminasameExcep() throws Exception {
		Studente studente=new Studente("Pierluigi","Sfregola",655689);
		studente.eliminaEsame(1);
		
	}
	
	
	@Test
	public void testMediaPesata() throws Exception {
		
		Studente studente=new Studente("Pierluigi","Sfregola",655689);
		studente.addEsame("piu",30,false,9);
		
		assertEquals(30.0,studente.getMediaPesata(),0.001);
		
		
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void testMediaPesataExcep() throws Exception {
		Studente studente=new Studente("Pierluigi","Sfregola",655689);
		studente.getMediaPesata();
		
	}
	
	
	
	
	

	

		
	

}
