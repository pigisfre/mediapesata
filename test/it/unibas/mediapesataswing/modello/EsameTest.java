package it.unibas.mediapesataswing.modello;

import static org.junit.Assert.*;

import org.junit.Test;

public class EsameTest {

	@Test
	public void setVotoTest() {
		Esame esame=new Esame ("analisi",19,false,9);
		assertEquals(19,esame.getVoto());
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void setVotoTestMin() throws Exception {
		Esame esame=new Esame ("analisi",9,false,9);
		assertEquals(9,esame.getVoto());
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void setVotoTestMax() throws Exception {
		Esame esame=new Esame ("analisi",31,false,9);
		assertEquals(31,esame.getVoto());
	}
	
	
	@Test
	public void setCreditiTest() {
		Esame esame=new Esame ("analisi",19,false,9);
		assertEquals(9,esame.getCrediti());
	}
	
	
	
	@Test(expected = IllegalArgumentException.class)
	public void setCreditiTestMin() throws Exception {
		Esame esame=new Esame ("analisi",20,false,0);
		assertEquals(0,esame.getCrediti());
		
	}
	
	
	@Test
	public void setLodeTest() {
		Esame esame=new Esame ("analisi",30,true,9);
		esame.setLode(false);
		assertFalse(esame.isLode());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setLodeIllegal() throws Exception {
		Esame esame=new Esame ("analisi",20,true,8);
		assertTrue(esame.isLode());
		
	}
	
	@Test
	public void toStringTestLode() {
		Esame esame=new Esame ("analisi",30,true,9);
		assertEquals( esame.toString(),"Esame di analisi (9 CFU) - voto: 30 e lode" );
	}
	
	@Test
	public void toStringTest() {
		Esame esame=new Esame ("analisi",30,false,9);
		assertEquals( esame.toString(),"Esame di analisi (9 CFU) - voto: 30" );
	}
	
	
	

}
